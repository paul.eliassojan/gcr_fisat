import React, { useState, useEffect} from "react";
import Modal from "./modal";
import axios from "axios";
import { css } from "@emotion/core";
import SyncLoader from "react-spinners/SyncLoader";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Link } from "react-router-dom";
import "../css/style.css";

const override = css`
  margin-left: 10px;
`;

const Gcr = () => {
  const [isModalOpen, toggleModal] = useState(false);
  const [clicked, setClicked] = useState(false);
  const [values, setValues] = useState({
    email: "",
  });
  const [resData, setResData] = useState("");
  let [color, setColor] = useState("white");
  let [loading, setLoading] = useState(false);

  const handleEmailInputChange = (event) => {
    event.persist();
    setValues((values) => ({
      ...values,
      email: event.target.value,
    }));
    setResData("");
  };
  const handleSubmit = () => {
    if (values.email.length > 3) {
      setClicked(true);
    }
  };
  useEffect(() => {
    if (clicked) {
      setLoading(true);
      const response = axios
        .post("https://gcr-fisat-server.herokuapp.com/email", values)
        .then((res) => {
          //console.log(res.data);
          setResData(res.data);
          toggleModal(!isModalOpen);
          setLoading(false);
        })
        .catch((error) => {
          setLoading(false);
          toast.error("Email is wrong", {
            position: "bottom-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        });

      setClicked(false);
    }
  });
  return (
    <div>
      <div class="title fixed-top">
        <h1 class="title_style">Welcome To GCR FISAT,</h1>
      </div>

      <form onSubmit={(e) => e.preventDefault()} role="form">
      <div style={{ marginTop: "5rem" }} class="container ">
        <div class="row">
          <div class="col-sm">
              <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input
                  type="email"
                  required
                  class="form-control"
                  placeholder="name@example.com"
                  name="email"
                  value={values.email}
                  onChange={handleEmailInputChange}
                />
                <button
                  onClick={() => {
                    handleSubmit();
                  }}
                  type="submit"
                  class="btn btn-primary"
                >
                  Search
                  <SyncLoader
                    color={color}
                    loading={loading}
                    css={override}
                    size={8}
                  />
                </button>
              </div>
          </div>
          <Modal isOpen={isModalOpen} toggle={toggleModal}>
            <h1>Hai, {resData.name}</h1>
          </Modal>
          <ToastContainer />
        </div>
      </div>
      </form>

      <div id="footer">
        <div class="container">
          <div class="row justify-content-center footer-block">
            <div class="row">
              <div class="col">Made With Love ❤️ .</div>
            </div>

            <div class="col">
              Developed by{" "}
              <a
                style={{ color: "inherit" }}
                target="_blank"
                href="https://www.linkedin.com/in/paul-elias-sojan/"
              >
                Paul Elias Sojan
              </a>
            </div>
          </div>
        </div>
      </div>
      </div>
  );
};

export default Gcr;
